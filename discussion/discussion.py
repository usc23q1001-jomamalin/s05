# [SECTION] Python Class Review
class SampleClass():
	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f"The year is: {self.year}")

myObj = SampleClass(2020)
print(myObj.year)
myObj.show_year()

# [SECTION] Fundamentals of OOP
# There are four main fundamental principles in OOP
# Encapsulation, Inheritance, Polymorphism, Abstraction

# [SECTION] Encapsulation
# Encapsulation - is a mechanism of wrapping the attributes and codes acting on the methods together as a single unit
# "data hiding"
# The prefix underscore(_) is used as a warning for developers that means: Please be careful about this attribute or method, dont use it outside the declared class.
class Person():
	def __init__(self):
		# protected attribute _name
		self._name = "John Doe"
		self._age = 18

	# setter method
	def set_name(self, name):
		self._name = name

	# getter method
	def get_name(self):
		print(f"Name of person: {self._name}")

	def set_age(self, age):
		self._age = age

	def get_age(self):
		print(f"Age of person: {self._age}")

p1 = Person()
# print(p1.name) - this will return an attribute error
p1.get_name()
p1.set_name("Jane Smith")
p1.get_name()

# Mini Exercise
# Add another protected attribute called "age" and create the necessary getter and setter methods
p1.get_age()
p1.set_age(40)
p1.get_age()

# [SECTION] Inheritance
# The transfer of the characteristics of a parent class to child classes are derived from it.
# "Parent-Child relationship"
# To create an inherited class, in the className definition with add the parent class as the parameter of the child class.
class Employee(Person):
	def __init__(self, employeeId):
		# super() can be used to invoke the immediate parent class constructor.
		super().__init__()
		# unique attribute to the Employee class
		self._employeeId = employeeId

	# Methods of the Employee class
	def get_employeeId(self):
		print(f"The Employee ID is {self._employeeId}")

	def set_employeeId(self, employeeId):
		self._employeeId = employeeId

	# Details method
	def get_details(self):
		print(f"{self._employeeId} belongs to {self._name}")

emp1 = Employee("Emp-001")
emp1.get_details()
emp1.get_name()
emp1.get_age()
emp1.set_name("Jane Smith")
emp1.set_age(40)
emp1.get_details()

# Mini exercise
# 1. Create a new class called Student that inherits Person with the additional attributes & methods
# Attributes: Student No., Course, Year level
# Methods: Create the necessary getters & setters for each attribute
# get_details: prints the output "<Student name> is currently in year <year level> taking up <Course>"
class Student(Person):
	def __init__(self, studentNum, course, year_lvl):
		super().__init__()
		self._studentNum = studentNum
		self._course = course
		self._year_lvl = year_lvl

	def set_studentNum(self, studentNum):
		self._studentNum = studentNum

	def set_course(self, course):
		self._course = course

	def set_yearlvl(self, year_lvl):
		self._year_lvl = year_lvl

	def get_studentNum(self):
		print(f"Student number of student is: {self._studentNum}")

	def get_course(self):
		print(f"Course of student is: {self._course}")

	def get_yearlvl(self):
		print(f"Year level of student is: {self._year_lvl}")

	def get_details(self):
		print(f"{self._name} is currently in year {self._year_lvl} taking up {self._course}")

student1 = Student("stud-001", "IT", 3)
student1.get_details()

# [SECTION] Polymorphism
# There are instances that the class is not always fit for the child class.
# Re-implementation/Overriding of method can be done in the child class
class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print("Admin User")

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print("Customer user")

def test_function(obj):
	obj.is_admin()
	obj.user_type()

# Create object instance of admin and customer
user_admin = Admin()
user_customer = Customer()
# Pass the created obj to test function
test_function(user_admin)
test_function(user_customer)


# Polymorphism with class methods
# Python uses two different class types in the same way.
class TeamLead():
	def occupation(self):
		print("Team Lead")

	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print("Team Member")

	def hasAuth(self):
		print(False)

tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()


# Polymorphism with Inheritance
# Polymorphism in python defines methods in child class that have the same name as methods in the parent class.
# "Method Overriding"
class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks(developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn web development in 360 hours!")

class DeveloperCareer(Zuitt):
	# override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn the basics of web development in 240 hours!")

class PiShapedCareer(Zuitt):
	# override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn the skills for no-code app development in 140 hours!")

class ShortCourses(Zuitt):
	# override the parent's num_of_hours() method
	def num_of_hours(self):
		print("Learn advanced topics in web development in 20 hours!")

course0 = Zuitt()
course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourses()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()
course0.num_of_hours()

# [SECTION] Abstraction
# An abstract class can be considered as a blueprint for other class.

# Abstract base classes (abc)
# The import tells the program to get the abc module of python to be used
from abc import ABC, abstractclassmethod

# The class polygon inherits the abstract class module.
class Polygon(ABC):
	# Created an abstract method called print_number_of_sides that needs to be implemented
	@abstractclassmethod
	def print_number_of_sides(self):
		# This denotes that the method doesn't do anything.
		pass

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has 3 sides")

class Pentagon(Polygon):
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has 5 sides")

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()
